'use strict';
/*global angular, d3*/

angular.module('ngWordCloud', [])
  .directive('wordcloud', [function() {
    return {
      link: function(scope, element, attrs) {

        var render = function () {

          var words = scope.$eval(element.attr('words'));
          words = words.slice(1, 20);

          // compute the size of each word
          var sum = 0;
          for (var i = 0; i < words.length; i++){
            sum += words[i].count;
          }
          for (var j = 0; j < words.length; j++){
            words[j].size = words[j].count / sum * 500;
          }

          function draw(words) {
            d3.select(element[0]).append('svg')
                .attr('width', 300)
                .attr('height', 300)
                .append('g')
                .attr('transform', 'translate(150,150)')
                .selectAll('text')
                .data(words)
                .enter().append('text')
                .style('font-size', function(d) { return d.size + 'px'; })
                .style('font-family', 'Impact')
                .style('fill', function(d, i) { return fill(i); })
                .attr('text-anchor', 'middle')
                .attr('transform', function(d) {
                  return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
                })
                .text(function(d) { return d.text; })
                .on('click', function(d) {console.log(d.text);});
          }

          var fill = d3.scale.category20();
          d3.layout.cloud().size([300, 300])
              .words(words)
              .padding(5)
              .rotate(function() { return Math.floor((Math.random() * 2)) * 90; })
              .font('Impact')
              .fontSize(function(d) { return d.size; })
              .on('end', draw)
              .start();
        };

          render();
          scope.$watch(function () {
              return scope.$eval(element.attr('words'));
          }, function () {
              render();
          });
      }
    };
  }]);

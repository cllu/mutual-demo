var _ = require('lodash');
var express = require('express');
var router = express.Router();

var twitter = require('../lib/twitter');
var text = require('../lib/text');

/**
 * Profile for a single user
 *
 * pass the twitter screen_name as one, like https://mutual-demo.herokuapp.com/sole?one=google
 */
router.get('/api/sole/', function (req, res) {
    var one = req.query.one;
    if (!one) {
        res.status(400).json({message: 'no twitter screen_name is specified'});
    }

    twitter.getTweetsWordCount(one, function (err, wordCount) {
        if (err) {
            res.send(err);
            throw err;
        }

        res.json({words: wordCount});
        console.log('request complete');
    });
});

/**
 * Mutual stuff between two users
 *
 * pass the twitter screen_name as one and another, for example:
 *   https://mutual-demo.herokuapp.com/sole?one=google&another=apple
 */
router.get('/api/mutual/', function (req, res) {
    var one = req.query.one;
    var another = req.query.another;

    twitter.getTweetsWordCount(one, function (err, oneWordCount) {
        if (err) {
            res.send(err);
            throw err;
        }

        twitter.getTweetsWordCount(another, function (err, anotherWordCount) {

            if (err) {
                res.send(err);
                throw err;
            }

            res.json({
                one: {
                    screenName: one,
                    words: oneWordCount
                },
                another: {
                    screenName: another,
                    words: anotherWordCount
                }
            });
            console.log('request complete');
        });
    });
});


module.exports = router;

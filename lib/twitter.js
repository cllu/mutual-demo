var Twitter = require('twitter');
var _ = require('lodash');
var text = require('./text');

// twitter client
var twitter = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token_key: process.env.TWITTER_ACCESS_TOKEN,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

var getTweets = exports.getTweets = function (screenName, callback) {

    var params = {
        trim_user: true,
        screen_name: screenName
    };

    twitter.get('statuses/user_timeline', params, function(error, tweets, response){
        if (error) {
            callback(error);
        } else {
            callback(null, tweets);
        }
    });
};

var getTweetText = exports.getTweetText = function (tweets) {
    return _.map(tweets, function (tweet) {
        return tweet.text;
    });
};

exports.getTweetsWordCount = function (screenName, callback) {

    getTweets(screenName, function (err, tweets) {
        if (err) {
            callback(err);
            throw err;
        }

        var sentences = getTweetText(tweets);
        var wordCount = text.wordCount(text.tokenize(sentences.join(' ')));
        callback(null, wordCount);
    });
};

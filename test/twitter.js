
var expect = require('chai').expect;

describe('twitter', function () {

    var twitter = require('../lib/twitter');

    it('getTweetText', function () {
        var tweets = [{text: 'text 1'}, {text: 'text 2'}];
        expect(twitter.getTweetText(tweets)).to.be.eql(['text 1', 'text 2']);
    })
});

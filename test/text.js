var expect = require('chai').expect;

describe('text', function () {

    var text = require('../lib/text');

    it('tokenize sentences', function () {
        expect(text.tokenize('I have a dream')).to.be.eql(['I', 'dream']);
    })
});
